import playerService from '../../src/service/player-service';
import playerDao from '../../src/dao/player-dao';

var player = {
    id: 'bfde44a9-bb0c-423e-8210-e2c69a7b5f48',
    username: 'sangoku',
    firstName: 'San',
    lastName: 'Go Ku',
    sex: 'M',
    power: '100',
};

describe('Player Service', function () {
    describe('Player', function () {
        describe('GIVEN I am customer', function () {
            it('WHEN I get a player ' + 'THEN the player is return', async () => {
                spyOn(playerDao, 'findByPk').and.callFake(function () {
                    return player;
                });

                let resPlayer = playerService.findByPk('1').then((resPlayer) => {
                    expect(resPlayer).not.toBeNull();
                    expect(resPlayer.username).toEqual('sangoku');
                    expect(resPlayer.firstName).toEqual('San');
                });
            });
        });
    });
});
