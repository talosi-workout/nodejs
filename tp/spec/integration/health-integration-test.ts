import supertest from 'supertest';
import app from '../../src/app';

const healthResponseOk: any = require('../data/health.json');

describe('Health Controller', function () {
    describe('GIVEN I am an api client', function () {
        it('WHEN I call the the api ' + 'THEN the api respond 200', async () => {
            await supertest(app)
                .get('/health')
                .expect(200)
                .then((response: any) => {
                    expect(response.body).toEqual(healthResponseOk);
                });
        });
    });
});
