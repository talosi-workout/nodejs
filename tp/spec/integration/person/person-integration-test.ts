import supertest from 'supertest';
import sequelize from '../../../src/dao/sequelize';
import Player from '../../../src/model/player';
//import personIntegrationTestUtils from "./person-integration-test-utils"
import app from '../../../src/app';

beforeEach(async () => {
    for (let modelName of Object.keys(sequelize.models)) {
        await sequelize.models[modelName].sync();
    }
});

describe('Player Controller', function () {
    describe('Player', function () {
        describe('GIVEN I am an api client', function () {
            it('WHEN I get an unknown player ' + 'THEN the api respond NOT FOUND', async () => {
                let response = await supertest(app)
                    .get(`/players/1`)
                    .set({ authorization: 'bearer' })
                    .send()
                    .then((response: any) => {
                        if (response.status != 200) {
                            console.error('api result', response.body);
                        }
                        return response;
                    });

                expect(response.status).toEqual(404);
            });
        });
    });
});
