# Nodejs Example application

Application propose to play at a Roleplay.

It can add several player, and organize a fight between players

## Run the application locally

fullly docker manage

```
docker compose up
```

Or with you self node

```
docker compose up -d  training-express-data
npm i
DATABASE_URI="postgres://training-express:training-express@localhost:5432/training-express" npm start
```

Application available on http://localhost:8080/health
swagger available on http://localhost:8080/api-docs.json
You can use the swagger ui on http://localhost:9098 with

```bash
docker run -p 9098:8080 -e URLS="[ { url: \"http://localhost:8080/api-docs.json\", name: \"tp-nodejs\" } ]" swaggerapi/swagger-ui
```

## api of this application

-   GET /players : display all players
-   PUT /players : add a player
-   PATCH /players/:id : partial update a player
-   DELETE /players/:id : delete a player
-   POST /fight : organize a fight between players

## Some curl to test the application

```bash
curl --request GET \
  --url http://localhost:8080/players \
  --header 'Authorization: ezf'

curl --request PUT \
  --url http://localhost:8080/players \
  --header 'Authorization: ezf' \
  --header 'Content-Type: application/json' \
  --data '{
	"username": "sangoku",
	"firstName": "San",
	"lastName" : "Go Ku",
	"sex": "M",
	"power": 100
}'

curl --request PUT \
  --url http://localhost:8080/players \
  --header 'Authorization: ezf' \
  --header 'Content-Type: application/json' \
  --data '{
	"username": "sankukai",
	"firstName": "San",
	"lastName" : "ku kai",
	"sex": "M",
	"power": 80
}'

curl --request PATCH \
  --url http://localhost:8080/players/948c2f80-0384-4871-b04b-aa62e383af64 \
  --header 'Authorization: ezf' \
  --header 'Content-Type: application/json' \
  --data '{
	"firstName": "Sandrine",
	"sex": "F"
}'


curl --request POST \
  --url http://localhost:8080/figths \
  --header 'Authorization: ezf' \
  --header 'Content-Type: application/json' \
  --data '{
	"teams" : [
		{
			"name" : "A",
			"players" : [
				{"username":"sangoku"}
			]
		},
			{
			"name" : "B",
			"players" : [
				{"username":"sankukai"}
			]
		}
	]
}'

```

//"npm:@vscode/sqlite3@^5.0.7",
