import Player from '../model/player';
import PlayerDao from '../dao/player-dao';
import Figth from '../types/figth';
import Team from '../types/team';

export default {
    async figth(figth: Figth) {
        const teams = await fillPlayersOnTeam(figth);

        teams.forEach((t: Team) => {
            t.power = t.players.map((p: Player | null) => Math.random() * (p?.power || 10)).reduce((p1: number, p2: number) => p1 + p2, 0);
        });

        teams.sort((t1: any, t2: any) => t2.power - t1.power);

        return {
            teams,
            winner: teams[0]?.name,
        };
    },
};

async function fillPlayersOnTeam(figth: Figth): Promise<Team[]> {
    return await Promise.all(
        figth.teams.map(async (t: Team) => ({
            ...t,
            players: await Promise.all(t.players.map((p: Player | null) => PlayerDao.findByUsername(p?.username))),
        })),
    );
}
