import Player from '../model/player';
import PlayerDao from '../dao/player-dao';
import { v4 as uuidv4 } from 'uuid';
import sequelize from '../dao/sequelize';

export default {
    async findAll() {
        return PlayerDao.findAll();
    },
    async findByPk(id: string) {
        return PlayerDao.findByPk(id);
    },
    async create(player: Player): Promise<Player> {
        player.id = uuidv4();

        return await sequelize.transaction(async (t) => {
            const playerResult: Player = await PlayerDao.create(player, t);

            return await PlayerDao.findByPk(playerResult.id, t);
        });
    },
    async update(player: Player): Promise<Player> {
        return await sequelize.transaction(async (t) => {
            return await PlayerDao.partialUpdate(player, t);
        });
    },
    async delete(id: string): Promise<Player> {
        const p = await PlayerDao.findByPk(id);

        await sequelize.transaction(async (t) => {
            await PlayerDao.delete(id, t);
        });
        return p;
    },
};
