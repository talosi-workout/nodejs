import { Request, Response, Router } from 'express';
import expressUtils from '../express/express-utils';
import joiMiddleware from '../express/joi-middleware';
import Figth, { figthValidator } from '../types/figth';
import figthService from '../service/figth-service';
const router = Router();

/**
 * @swagger
 * /figths:
 *    post:
 *      description: organize a fight between players. Only username are necessary on Player object
 *      tags:
 *      - figth
 *      responses:
 *        "200":
 *          content:
 *            application/json:
 *              schema:
 *                type: array
 *                items:
 *                  $ref: '#/components/schemas/Figth'
 *
 */
router.post(
    '/figths',
    joiMiddleware(figthValidator()),
    expressUtils.catchAsyncError(async (req: Request, res: Response) => {
        const figth: Figth = req.body;

        return res.send({
            data: await figthService.figth(figth),
        });
    }),
);

module.exports = router;
export default module.exports;
