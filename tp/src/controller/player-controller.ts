import { Request, Response, Router } from 'express';
import { playerValidator } from '../model/player';
import PlayerService from '../service/player-service';
import expressUtils from '../express/express-utils';
import joiMiddleware from '../express/joi-middleware';
const router = Router();

/**
 * @swagger
 * /player:
 *    get:
 *      description: get all player, or filter a list of person
 *      tags:
 *      - player
 *      responses:
 *        "200":
 *          content:
 *            application/json:
 *              schema:
 *                type: array
 *                items:
 *                  $ref: '#/components/schemas/Player'
 *
 */
router.get(
    '/players',
    expressUtils.catchAsyncError(async (req: Request, res: Response) => {
        const persons = await PlayerService.findAll();
        return res.send({
            data: persons,
        });
    }),
);

/**
 * @swagger
 * /player/{id}:
 *    get:
 *      description: get player detail
 *      tags:
 *      - player
 *      parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *          required: true
 *      responses:
 *        "200":
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Player'
 *        "404":
 *          $ref: '#/components/HttpException'
 *        "500":
 *          $ref: '#/components/HttpException'
 *
 */
router.get(
    '/players/:id',
    expressUtils.catchAsyncError(async (req: Request, res: Response) => {
        const id = req.params.id;
        return res.send({
            data: await PlayerService.findByPk(id),
        });
    }),
);

/**
 * @swagger
 * /player:
 *    put:
 *      description: create a person
 *      tags:
 *      - person
 *      parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *          required: true
 *      - in: data
 *        name: user
 *        description: The user to create.
 *        schema:
 *          $ref: '#/components/schemas/Player'
 *      responses:
 *        "200":
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Player'
 *        "404":
 *          $ref: '#/components/HttpException'
 *        "500":
 *          $ref: '#/components/HttpException'
 *
 */
router.put(
    '/players',
    joiMiddleware(playerValidator(false)),
    expressUtils.catchAsyncError(async (req: Request, res: Response) => {
        return res.send({
            data: await PlayerService.create(req.body),
        });
    }),
);

/**
 * @swagger
 * /person:
 *    patch:
 *      description: update a player partially. Possible to use only one attribute to update
 *      tags:
 *      - player
 *      parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *          required: true
 *      - in: body
 *        name: user
 *        description: The user to create.
 *        schema:
 *          $ref: '#/components/schemas/Player'
 *      responses:
 *        "200":
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Player'
 *        "404":
 *          $ref: '#/components/HttpException'
 *        "500":
 *          $ref: '#/components/HttpException'
 *
 */
router.patch(
    '/players/:id',
    joiMiddleware(playerValidator(true)),
    expressUtils.catchAsyncError(async (req: Request, res: Response) => {
        req.body.id = req.params.id;
        return res.send({
            data: await PlayerService.update(req.body),
        });
    }),
);

/**
 * @swagger
 * /player:
 *    delete:
 *      description: delete player
 *      tags:
 *      - person
 *      parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *          required: true
 *      responses:
 *        "200":
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Player'
 *        "404":
 *          $ref: '#/components/HttpException'
 *        "500":
 *          $ref: '#/components/HttpException'
 *
 */
router.delete(
    '/players/:id',
    joiMiddleware(playerValidator(false)),
    expressUtils.catchAsyncError(async (req: Request, res: Response) => {
        const id = req.params.id;
        return res.send({
            data: await PlayerService.delete(id),
        });
    }),
);

module.exports = router;
export default module.exports;
