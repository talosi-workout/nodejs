import { Request, Response, Router } from 'express';
import expressUtil from '../express/express-utils';
import sequelize from '../dao/sequelize';

const router = Router();

/**
 * @swagger
 * /health:
 *    get:
 *      description: Health check with database verification
 *      tags:
 *      - health
 *    produces:
 *       - application/json
 */
router.get(
    '/health',
    expressUtil.catchAsyncError(async (req: Request, res: Response) => {
        let databaseStatus = 'UNKNOWN';
        let error = undefined;
        try {
            await sequelize.authenticate();
            databaseStatus = 'UP';
        } catch (err) {
            databaseStatus = 'DOWN';
            error = err.message;
        }
        return res.send({
            server: {
                status: 'UP',
            },
            database: {
                status: databaseStatus,
                error,
            },
        });
    }),
);

module.exports = router;
export default module.exports;
