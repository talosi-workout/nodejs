import { Sequelize } from 'sequelize-typescript';
import Player from '../model/player';

let databaseUri = process.env.DATABASE_URI;
if (databaseUri == null) databaseUri = 'sqlite::memory:';

export default new Sequelize(databaseUri, {
    define: {
        timestamps: false,
    },
    models: [Player],
});
