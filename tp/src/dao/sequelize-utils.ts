import { Transaction } from 'sequelize';
import { Model } from 'sequelize-typescript';

export default {
    async updatePartialObject<M extends Model>(model: M, partialObject: any, t: Transaction | null = null): Promise<M> {
        const fieldsToUpdate = Object.keys(partialObject);

        const modelCopy = Object.create(model);

        fieldsToUpdate.forEach((field) => {
            // @ts-ignore
            modelCopy[field] = partialObject[field];
        });
        // @ts-ignore
        return await modelCopy.save({ fields: fieldsToUpdate, transaction: t });
    },
};
