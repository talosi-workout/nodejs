import Player from '../model/player';
import NotFoundException from '../exception/notfound-exception';
import { Transaction } from 'sequelize';
import SequelizeUtils from './sequelize-utils';

export default {
    async findAll(): Promise<Player[]> {
        const players = await Player.findAll();
        return players;
    },
    async findByUsername(username: string | undefined): Promise<Player | null> {
        const player: Player | null = await Player.findOne({
            where: {
                username,
            },
        });
        return player;
    },
    async findByPk(id: string | undefined, t: Transaction | null = null): Promise<Player> {
        const player = await Player.findByPk(id, {
            transaction: t,
        });
        if (player == null) throw new NotFoundException(`Player with id ${id} not found`);
        return player;
    },
    async create(player: Player, t: Transaction | null = null): Promise<Player> {
        const playerCreated = await Player.create({ ...player }, { transaction: t });

        return playerCreated;
    },
    async partialUpdate(partialPlayer: Player, t: Transaction | null = null): Promise<Player> {
        const player = await this.findByPk(partialPlayer.id, t);
        await SequelizeUtils.updatePartialObject(player, partialPlayer, t);

        return this.findByPk(player.id, t);
    },
    async delete(id: string, t: Transaction | null = null): Promise<void> {
        await Player.destroy({ where: { id }, transaction: t });
    },
};
