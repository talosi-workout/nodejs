/**
 * @swagger
 * components:
 *   HttpException:
 *     content:
 *       application/json:
 *         schema:
 *           type: object
 *           properties:
 *             error:
 *               type: string
 *
 */
export default class HttpException extends Error {
    code: number;
    constructor(code: number, msg: string) {
        super(msg);
        this.code = code;
    }
}
