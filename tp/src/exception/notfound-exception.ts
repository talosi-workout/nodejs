import HttpException from './http-exception';

export default class NotFoundException extends HttpException {
    constructor(msg: string) {
        super(404, msg);
    }
}
