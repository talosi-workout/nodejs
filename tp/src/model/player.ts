import { Column, DataType, Table, Model } from 'sequelize-typescript';
import Joi from '@hapi/joi';
import joiUtils from '../utils/joi-utils';

/**
 * @swagger
 * components:
 *   schemas:
 *     Player:
 *       type: object
 *       required:
 *       - firstName
 *       - lastName
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the player.
 *         username:
 *           type: string
 *           description: pseudo of the player
 *         firstName:
 *           type: string
 *           description: First name of the player.
 *         lastName:
 *           type: string
 *           description: Last name of the player.
 *         sex:
 *           type: string
 *           description: sex of the player.
 *         power:
 *           type: number
 *           description: power of the player.
 */
@Table({
    tableName: 'player',
})
export default class Player extends Model {
    @Column({ type: DataType.UUID, primaryKey: true })
    id: string | undefined;

    @Column({ type: DataType.STRING, field: 'username' })
    username: string | undefined;

    @Column({ type: DataType.STRING, field: 'first_name' })
    firstName: string | undefined;

    @Column({ type: DataType.STRING, field: 'last_name' })
    lastName: string | undefined;

    @Column({ type: DataType.STRING, field: 'sex' })
    sex: 'F' | 'M' | undefined;

    @Column({ type: DataType.STRING, field: 'power' })
    power: number | undefined;
}

function playerValidator(partialUpdate: boolean = false) {
    return Joi.object().keys({
        id: Joi.string(),
        username: joiUtils.requiredJoi(Joi.string(), !partialUpdate),
        firstName: joiUtils.requiredJoi(Joi.string(), !partialUpdate),
        lastName: joiUtils.requiredJoi(Joi.string(), !partialUpdate),
        sex: joiUtils.requiredJoi(Joi.string().valid('F', 'M'), !partialUpdate),
        power: joiUtils.requiredJoi(Joi.number(), !partialUpdate),
    });
}

export { playerValidator };
