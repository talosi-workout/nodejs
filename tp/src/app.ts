import { Transaction } from 'sequelize';
'use strict';

import express from 'express';
import url from 'url';
import { NextFunction, Request, Response, Router } from 'express';
import healthEndpoint from './controller/health-controller';
import log from './config/logger-config';
import auth from './config/authorization-config';
import swaggerExpress from './express/swaggerExpress';
import expressUtils from './express/express-utils';
import cors from 'cors';
import bodyParser from 'body-parser';
import playerController from './controller/player-controller';
import figthController from './controller/figth-controller';
import sequelize from './dao/sequelize';

const app = express();

app.disable('x-powered-by');
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// open api (no need bearer)
app.use('/', healthEndpoint);
app.use('/', swaggerExpress);

app.use('/', (req: Request, res: Response, next: NextFunction) => {
    let pathname = url.parse(req.path).pathname || '';
    if (pathname.endsWith('/exception')) {
        next();
    } else {
        return auth.grantAccess(req, res, next);
    }
});

app.use('/', playerController);
app.use('/', figthController);

// add here your secured endpoint
// app.use('/', exampleEndpoint);

app.use(expressUtils.genericsErrorHandler);

// Start the server
const PORT = process.env.PORT || 8080;

sequelize.sync({ force: true }).then(() => {
    console.log('All models were synchronized successfully.');
});

// handle exception
process.on('unhandledRejection', (error: any) => {
    log.error('unhandledRejection : ' + error.stack);
});

module.exports = app.listen(PORT);

export default module.exports;
