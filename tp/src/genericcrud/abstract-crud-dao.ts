import { Transaction } from 'sequelize';
import { Model } from 'sequelize-typescript';
import { StatusCodes } from 'http-status-codes';
import NotFoundException from '../exception/notfound-exception.js';
import HttpException from '../exception/http-exception.js';

export default abstract class AbstractCrudDao<E extends Model> {
    abstract getModel(): typeof Model;
    // eslint-disable-next-line class-methods-use-this
    eager(): (typeof Model)[] {
        return [];
    }

    async findAll(): Promise<E[]> {
        // @ts-ignore
        return this.getModel().findAll();
    }

    async findByPk(id: number | string | undefined, t: Transaction | null = null): Promise<E> {
        // @ts-ignore
        const entity: E = await this.getModel().findByPk(id, { include: this.eager(), transaction: t });
        if (entity == null) throw new NotFoundException(`Object with id ${id} not found`);
        return entity;
    }

    async create(plan: E, t: Transaction | null = null): Promise<E> {
        try {
            // @ts-ignore
            const playerCreated: E = await this.getModel().create({ ...plan }, { include: this.eager(), transaction: t });
            return playerCreated;
        } catch (e: any) {
            if (e?.name === 'SequelizeUniqueConstraintError') {
                throw new HttpException(StatusCodes.CONFLICT, `Object with id ${plan.id} alreadyt exist`);
            }
            throw e;
        }
    }

    async partialUpdate(partialPlan: E, t: Transaction | null = null): Promise<E> {
        const entity = await this.findByPk(partialPlan.id, t);

        await entity.update({ ...partialPlan });

        return this.findByPk(partialPlan.id, t);
    }

    async delete(id: number, t: Transaction | null = null): Promise<void> {
        // @ts-ignore
        await this.getModel().destroy({ where: { id }, transaction: t });
    }
}
