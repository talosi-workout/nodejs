import { Model } from 'sequelize-typescript';
import AbstractCrudDao from './abstract-crud-dao.js';

export default abstract class AbstractCrudService<E extends Model> {
    abstract getDao(): AbstractCrudDao<E>;

    async findAll() {
        return this.getDao().findAll();
    }

    async findByPk(id: number | string | undefined): Promise<E> {
        return this.getDao().findByPk(id);
    }

    async create(entity: E): Promise<E> {
        return this.getDao().create(entity);
    }

    async update(entity: E): Promise<E> {
        return this.getDao().partialUpdate(entity);
    }

    async delete(id: number): Promise<void> {
        await this.getDao().delete(id);
    }
}
