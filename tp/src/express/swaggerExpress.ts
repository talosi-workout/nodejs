import swagger from './swagger';
import { Response, Router } from 'express';
import log from '../config/logger-config';

const router = Router();

try {
    router.get(swagger.SWAGGER_ENDPOINT, (req: any, res: Response) => {
        res.setHeader('Content-Type', 'application/json');

        if (req.scanFolder) res.send(swagger.getSwaggerResponse(req.scanFolder));
        else res.send(swagger.getSwaggerResponse());
    });
} catch (e) {
    log.warn("It's seems you haven't express library, so you can't use it");
}

module.exports = router;
export default module.exports;
