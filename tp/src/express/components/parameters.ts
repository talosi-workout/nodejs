/**
 * @swagger
 * components:
 *   parameters:
 *     authParams:
 *       name: Authorization
 *       description: Bearer auth to log user
 *       in: header
 *       required: true
 *       schema:
 *         type: string
 *         default: Bearer xxx
 *
 *     apiKeyParams:
 *       name: x-api-key
 *       description: Api key to securise api
 *       in: header
 *       required: true
 *       schema:
 *         type: string
 *         default: apikey
 */
