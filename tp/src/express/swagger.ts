import swaggerJsDoc from 'swagger-jsdoc';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const pjson = require(process.cwd() + '/package.json');

const config = [
    {
        url: 'http://localhost:8080/',
        description: 'Local staging',
    },
];

let swaggerResponse: any = null;

module.exports = {
    SWAGGER_ENDPOINT: '/api-docs.json',
    getSwaggerResponse(scanFolder = ['src/**/*.*s']) {
        if (swaggerResponse) return swaggerResponse;

        scanFolder.push(__dirname + '/components/parameters.js');

        const option = {
            definition: {
                openapi: '3.0.0', // Specification (optional, defaults to swagger: '2.0')
                info: {
                    title: pjson.name,
                    version: pjson.version,
                },
                servers: config,
            },
            apis: scanFolder,
        };

        swaggerResponse = swaggerJsDoc(option);
        return swaggerResponse;
    },
};

export default module.exports;
