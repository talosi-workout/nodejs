import errorHandler from './error-handler';
import log from '../config/logger-config';
import { Request, Response } from 'express';

// throw une erreur avec un async dans les controller ne fonctionne. Permet de le faire fonctionner
const catchAsyncError =
    (fn: any) =>
    (...args: any) =>
        fn(...args).catch(args[2]);

const genericsErrorHandler = (err: any, req: Request, res: Response) => {
    if (err.code == undefined || isNaN(err.code) || err.code >= 500) log.error(err.stack);
    errorHandler.manageErrorResponse(err, res);
};

module.exports = {
    catchAsyncError,
    genericsErrorHandler,
};

export default module.exports;
