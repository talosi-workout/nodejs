const middleware = (schema: any) => {
    return (req: any, res: any, next: any) => {
        const error = schema.validate(req.body);
        if (error.error == null) {
            next();
        } else {
            res.status(422).json({ error: error.error.message });
        }
    };
};
module.exports = middleware;
export default module.exports;
