import { Response } from 'express';

async function manageErrorResponse(err: any, res: Response) {
    if (err.code && !isNaN(err.code)) {
        try {
            res.status(err.code).send({
                error: err.message,
            });
        } catch (e) {
            res.status(500).send({
                error: err.message,
            });
        }
    } else {
        res.status(500).send({
            error: err.message,
        });
    }
}

module.exports = { manageErrorResponse };
export default module.exports;
