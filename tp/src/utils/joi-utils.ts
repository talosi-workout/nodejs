import { AnySchema } from '@hapi/joi';

export default {
    requiredJoi(schema: AnySchema, required: boolean): AnySchema {
        return required ? schema.required() : schema;
    },
};
