import HttpStatus from 'http-status-codes';
import { NextFunction, Request, Response } from 'express';
import log from './logger-config';

module.exports = {
    async grantAccess(req: Request, res: Response, next: NextFunction) {
        if (req.headers?.authorization) {
            const token = req.headers.authorization;

            try {
                // TODO test token here
                log.debug(`the token: ${token}`);

                next();
            } catch (error) {
                log.error(error);
                return res.status(HttpStatus.UNAUTHORIZED).json({ error: { msg: JSON.stringify(error) } });
            }
        } else {
            return res.status(HttpStatus.UNAUTHORIZED).json({ error: { msg: 'Missing token!' } });
        }
    },
};

export default module.exports;
