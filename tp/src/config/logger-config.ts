import { createLogger, transports, format } from 'winston';

const { combine, timestamp, printf } = format;
let transport = [];

const myFormat = printf(({ level, message, timestamp }: any) => {
    return `${timestamp} - ${level}: ${message}`;
});

const options = {
    console: {
        handleExceptions: true,
        json: false,
        colorize: true,
        format: combine(timestamp(), myFormat),
    },
};

transport = [new transports.Console(options.console)];

const log = createLogger({
    level: process.env.loggerLevel ? process.env.loggerLevel : 'info',
    transports: transport,
    exitOnError: false, // do not exit on handled exceptions
});

module.exports = log;
export default module.exports;
