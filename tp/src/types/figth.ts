import Joi from '@hapi/joi';
import Team, { teamValidator } from './team';

/**
 * @swagger
 * components:
 *   schemas:
 *     Figth:
 *       type: object
 *       required:
 *       - teams
 *       properties:
 *         teams:
 *           $ref: '#/components/schemas/Team'
 *           description: Team
 *         winner:
 *           type: string
 *           description: First name of the player.
 */

interface Figth {
    teams: Team[];
    winner: string;
}

export default Figth;

function figthValidator() {
    return Joi.object().keys({
        teams: Joi.array().items(teamValidator()),
    });
}

export { figthValidator };
