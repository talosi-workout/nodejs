import Joi from '@hapi/joi';
import Player from '../model/player';

/**
 * @swagger
 * components:
 *   schemas:
 *     Team:
 *       type: object
 *       required:
 *       - name
 *       - players
 *       properties:
 *         name:
 *           type: string
 *           description: name of the team
 *         players:
 *           $ref: '#/components/schemas/Player'
 *           description: players of the team
 *         power:
 *           type: number
 *           description: power win by the team
 */

interface Team {
    name: string;
    players: (Player | null)[];
    power: number;
}

export default Team;

function playerValidator() {
    return Joi.object().keys({
        username: Joi.string(),
    });
}

function teamValidator() {
    return Joi.object().keys({
        name: Joi.string(),
        players: Joi.array().items(playerValidator()),
    });
}

export { teamValidator };
