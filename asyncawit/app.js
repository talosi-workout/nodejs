function divide(a,b) {
  if(b === 0) return {sucess:false, error: "Division par 0 impossible"}
  return {success:true, data:a/b};
}

async function asycnDivide(a,b) {
  let result = divide(a,b);
  if(!result.success) throw new Error(result.error);
  return result.data;
}

asycnDivide(4,9)
  .then(  data => console.log("success", data))
  .catch( error => console.log("error", error));

asycnDivide(4,0)
  .then(  data => console.log("success", data))
  .catch( error => console.log("error", error));