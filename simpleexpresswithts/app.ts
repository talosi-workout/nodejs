import express from 'express';
import {Request, Response} from 'express';
import bodyParser from "body-parser";

const app = express();
const port:number = 8080;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.get('/', (req: Request, res: Response) => {
  res.send('Hello World!')
 });
 
 app.put('/players', (req: Request, res: Response) => {
    res.send(req.body)
 });
 

 app.listen(port, () => {
  console.log(`Application exemple à l'écoute sur le port ${port}!`)
 });