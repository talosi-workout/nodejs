const http = require('http');

const server = http.createServer((req, res) => {
    res.end('Councou! Bienvenue sur cet exercice.');
});

const port =  process.env.PORT || 3000;
server.listen(port)

console.log("Listen on port " + port + " (http://localhost:3000/)");